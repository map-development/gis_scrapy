#!/bin/bash
# 从第一行到最后一行分别表示：
# 1. 生成数据库迁移文件
# 2. 根据数据库迁移文件来修改数据库
# 3. 用 uwsgi启动 django 服务, 不再使用python manage.py runserver
# python manage.py makemigrations&&
#
# python manage.py migrate&&
#
python manage.py collectstatic --noinput
uwsgi --ini /home/gis_scrapy/uwsgi_docker.ini --enable-threads
# python manage.py runserver 0.0.0.0:8000

#修改配置文件 启动scrapyd
#/usr/local/lib/python3.7/site-packages/scrapyd/default_scrapyd.conf

