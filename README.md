# gis_scrapy

#### 介绍
舆情数据爬取

#### 软件架构
基于python3.7构建，python3.7是长时间支持版本的 最低版本，能支持到2023年，生态较好。

数据库 使用mongodb4.4 非关系型数据库，便于聚合操作


#### 安装教程

1.  安装依赖包 pip3 install -r requirements.txt
2.  运行项目 runserver 127.0.0.1:8080
3.  xxxx

#### 使用说明
1. scrapy 使用说明

    一套基于Twisted事件驱动的异步爬虫框架
    
    是为持续运行设计的专业爬虫框架,提供了操作的Scrapy命令行
    
    并发性好
    
    对于大场景支持良好
    
    集成部分反 反爬能力
    
```

# 创建项目 (一个项目只需执行一次，本项目已经执行)
scrapy startproject yu_qing

# 创建一个Spider （用于本地创建）
scrapy genspider movie movie.douban.com

# 运行 （用于本地测试）
scrapy crawl movie

# 启动scrapyd服务 (该服务已经启动，无需再启)
服务器配置文件位置 /usr/local/python3/lib/python3.7/site-packages/scrapyd/default_scrapyd.conf
启动：
/Users/cwy/Library/Python/3.7/bin/scrapyd

scrapyd-deploy -p yu_qing
```

2. mongodb 数据库
```
服务器 11.75.1.54:27017
账密 root 123456
```

3. mysql 数据库
```
服务器 11.75.1.54:3306
账密 root 123456
```

#### 参与贡献
```
git init
#添加当前文件夹下所有文件
git add .
git commit -m "first commit"
git remote add origin https://gitee.com/map-development/gis_scrapy.git
git push -u origin develop

#分支操作
git branch
git checkout develop
#删除远程文件
git rm -r --cached .idea
```



#### 部署过程
```
# 创建项目
django-admin startproject gis_scrapy
# 创建应用
python3 manage.py startapp apps
# 数据迁移
python3 manage.py migrate
```

#### 统一错误处理
