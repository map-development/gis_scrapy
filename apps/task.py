import time
import asyncio
import pika
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job

from apps.tools.common_tools import *
from apps.models.comment_scrapy import CommentMeituan
from apps.service.comment_service import CommentService
try:
    # 实例化调度器
    scheduler = BackgroundScheduler()
    # 调度器使用DjangoJobStore()
    scheduler.add_jobstore(DjangoJobStore(), "default")


    # 'cron'方式循环，周一到周五，每天9:30:10执行,id为工作ID作为标记
    # ('scheduler',"interval", seconds=1) #用interval方式循环，每一秒执行一次
    @register_job(scheduler, 'cron', day_of_week='mon-fri', hour='12', minute='30', second='10', id='task_time')
    # @register_job(scheduler, 'interval', id='test', hours=0, minutes=1)
    def test_job():
        t_now = time.localtime()
        print(t_now)


    @register_job(scheduler, 'interval', id='test', hours=0, minutes=0, seconds=10)
    def test_two():
        CommentService.set_count()
        # get_scrapyd_cli().schedule('yu_qing', 'movie')
        t_now = time.localtime()
        print(t_now)


    @register_job(scheduler, 'date', id='tt')  # 只执行一次的非阻塞异步任务 这里可以写rabbitmq的消费任务
    # @register_job(scheduler, 'interval', id='test', hours=0, minutes=0, seconds=1)
    def test_rabbitmq():
        connection = pika.BlockingConnection(settings.rabbitmq_connection_parameters)
        channel = connection.channel()

        channel.queue_declare(queue='hello')

        def callback(ch, method, properties, body):
            print(" [x] Received %r" % body)

        channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()
        t_now = time.localtime()
        print(t_now)

        #
        # connection.close()


    @register_job(scheduler, 'date', id='yq_meituan_comment')  # 只执行一次的非阻塞异步任务 这里可以写rabbitmq的消费任务
    # @register_job(scheduler, 'interval', id='test', hours=0, minutes=0, seconds=1)
    def consumer_mt_yq_comment():
        connection = pika.BlockingConnection(settings.rabbitmq_connection_parameters)
        channel = connection.channel()

        channel.queue_declare(queue='yq.mt.comment')

        def callback(ch, method, properties, body):
            result = json.loads(body)
            for key, value in enumerate(result):
                ota_spot_id = value['ota_spot_id']
                review_id = value['review_id']

                user_name = value['user_name']
                user_url = value['user_url']
                comment = value['comment']
                pic_urls = value['pic_urls']
                comment_time = value['comment_time']
                reply_cnt = value['reply_cnt']
                zan_cnt = value['zan_cnt']
                read_cnt = value['read_cnt']
                user_id = value['user_id']
                star = value['star']
                menu = value['menu']

                CommentMeituan.objects(ota_spot_id=ota_spot_id, review_id=review_id).update_one(
                    set__user_name=user_name,
                    set__user_url=user_url,
                    set__comment=comment,
                    set__pic_urls=pic_urls,
                    set__comment_time=comment_time,
                    set__reply_cnt=reply_cnt,
                    set__zan_cnt=zan_cnt,
                    set__read_cnt=read_cnt,
                    set__user_id=user_id,
                    set__star=star,
                    set__menu=menu,
                    upsert=True
                )

        channel.basic_consume(queue='yq.mt.comment', on_message_callback=callback, auto_ack=True)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()
        t_now = time.localtime()
        print(t_now)


    # 监控任务
    register_events(scheduler)
    # 调度器开始
    scheduler.start()
except Exception as e:
    print(e)
    # 报错则调度器停止执行
    scheduler.shutdown()
