# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  comment_view.py
@Description    :  
@CreateTime     :  2021/2/25 3:29 下午
------------------------------------
@ModifyTime     :  
"""
import json

from apps.service.comment_service import CommentService
from apps.tools.common_tools import success


def get_count(request):
    count = CommentService.get_count()
    return success(json.loads(count))
