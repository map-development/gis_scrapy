import json

import pika
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from apps.service.inventory_service import InventoryService
from apps.tools.common_tools import logger, success
from django.core.cache import cache

from gis_scrapy import settings


def index(request):
    # mongodb数据库操作示例
    InventoryService.create()
    return JsonResponse({"status": 200, "msg": "OK", "data": 1})


def test(request):
    # GET请求示例 http://127.0.0.1:8080/test/?a=1
    print(request.GET['a'])
    logger().info('这是一个日志')
    cache.set('test_key', 1, timeout=None)
    print(cache.get('test_key'))

    connection = pika.BlockingConnection(settings.rabbitmq_connection_parameters)
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='', routing_key='hello', body='Hello World!')
    print(" [x] Sent 'Hello World!'")
    connection.close()
    # logger().error('这是一个日志')
    return success([1, 2, 3, 4, 5, 6])
    # return JsonResponse({"status": 200, "msg": "OK", "data": 1})


@require_http_methods(["POST"])
def apis(request):
    # POST请求示例 http://127.0.0.1:8080/apis  参数：{"word":"data"}
    param = json.loads(request.body)
    print(param)
    result = "success"
    if request.method == "POST":
        print(param['word'])

    return JsonResponse({"status": 200, "msg": "OK", "data": result})

# def get_dict(key):
#     value = cache.get(key)
#     if value is None:
#         return None
#
#     return json.loads(value)
#
#
# def set_dict(key, value_dict):
#     return cache.set(key, json.dumps(value_dict))
