# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  comment_service.py
@Description    :  
@CreateTime     :  2021/2/25 2:33 下午
------------------------------------
@ModifyTime     :  
"""
import json

from apps.models.comment_scrapy import *
from django.core.cache import cache


class CommentService(object):
    @staticmethod
    def set_count():
        qunar_count = CommentQunar.objects.count()
        ctrip_count = CommentCtrip.objects.count()
        meituan_count = CommentMeituan.objects.count()
        lvmama_count = CommentLvmama.objects.count()
        ly_count = CommentLy.objects.count()

        total_count = qunar_count + ctrip_count + meituan_count + lvmama_count + ly_count

        comment_count = {'qunar_count': qunar_count, 'ctrip_count': ctrip_count, 'meituan_count': meituan_count,
                         'lvmama_count': lvmama_count, 'ly_count': ly_count, 'total_count': total_count}
        cache.set('gis_scrapy:comment_count', json.dumps(comment_count), timeout=None)
        print(comment_count)

    @staticmethod
    def get_count():
        return cache.get('gis_scrapy:comment_count')
