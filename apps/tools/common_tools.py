import json
import logging
import time
# import pika
from django.http import HttpResponse
from scrapyd_api import ScrapydAPI

# scrapyd = ScrapydAPI('http://127.0.0.1:6800')
# scrapyd.schedule('yu_qing', 'movie')

# scrapy 客户端
from apps.tools.service_code import ServiceCode
from gis_scrapy import settings


# -> 表示返回类型
def get_scrapyd_cli() -> ScrapydAPI:
    return ScrapydAPI(settings.SCRAPY_URL)


# 日志打印
def logger() -> logging:
    return logging.getLogger('gis_scrapy.log')


"""
成功返回方法
"""


def success(data: dict = {}, service_code: ServiceCode = ServiceCode.other_success) -> HttpResponse:
    response: dict = {'head': {'time': int(time.time()), 'code': service_code.value.code,
                               'message': service_code.value.msg}, 'data': data}
    content_type = 'application/json'
    return HttpResponse(json.dumps(response), content_type)


"""
失败返回方法
"""


def failure(service_code: ServiceCode = ServiceCode.other_failure, data: dict = None) -> HttpResponse:
    response: dict = {'head': {'time': int(time.time()), 'code': service_code.value.code,
                               'message': service_code.value.msg}, 'data': data}
    content_type = 'application/json'
    return HttpResponse(json.dumps(response), content_type)
