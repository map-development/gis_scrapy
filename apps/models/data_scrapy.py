import mongoengine
from mongoengine import Document


class DataQunar(Document):
    sight_id = mongoengine.IntField()  # 景区id
    comment_avg_score = mongoengine.StringField()
    tag_list = mongoengine.ListField()
    total = mongoengine.IntField()


class DataCtrip(Document):
    ota_spot_id = mongoengine.IntField()  # 景区id
    cmtquantity = mongoengine.IntField()
    cmtscore = mongoengine.FloatField()
    recompct = mongoengine.StringField()
    stscs = mongoengine.ListField()
    totalpage = mongoengine.IntField()


class DataMeituan(Document):
    ota_spot_id = mongoengine.IntField()
    tags = mongoengine.ListField()
    total = mongoengine.IntField()


class DataLvmama(Document):
    ota_spot_id = mongoengine.IntField()  # 景区id
    main_place_id = mongoengine.IntField()  # 景点地方id
