import mongoengine
from mongoengine import Document


class CommentQunar(Document):
    sight_id = mongoengine.IntField()
    comment_id = mongoengine.StringField()
    author = mongoengine.StringField()
    comment_quality = mongoengine.StringField()
    content = mongoengine.StringField()
    date = mongoengine.StringField()  # 2019-06-09
    head_img = mongoengine.StringField()
    imgs = mongoengine.ListField()
    product_id = mongoengine.StringField()
    score = mongoengine.FloatField()
    sight_name = mongoengine.StringField()
    supplier_id = mongoengine.StringField()
    supplier_name = mongoengine.StringField()
    tag_list = mongoengine.ListField()


class CommentCtrip(Document):
    ota_spot_id = mongoengine.IntField()
    bimgs = mongoengine.ListField()
    content = mongoengine.StringField()
    cost_performance_star = mongoengine.FloatField()
    date = mongoengine.StringField()  # 2020-12-30 12:03 评论日期
    id = mongoengine.IntField()  # 用户评论id
    interest_star = mongoengine.FloatField()
    score = mongoengine.StringField()  # 评分
    sight_star = mongoengine.FloatField()
    uid = mongoengine.StringField()
    user_image = mongoengine.StringField()


class CommentMeituan(Document):
    ota_spot_id = mongoengine.IntField()
    user_name = mongoengine.StringField()
    user_url = mongoengine.StringField()
    comment = mongoengine.StringField()
    pic_urls = mongoengine.ListField()
    comment_time = mongoengine.StringField()  # 评论时间
    reply_cnt = mongoengine.IntField()  # 回复数
    zan_cnt = mongoengine.IntField()  # 点赞数
    read_cnt = mongoengine.IntField()  # 阅读数
    user_id = mongoengine.StringField()
    star = mongoengine.IntField()  # 星星
    review_id = mongoengine.StringField()  # 貌似是评论id
    menu = mongoengine.StringField()  # 票


class CommentLvmama(Document):
    ota_spot_id = mongoengine.IntField()
    created_time = mongoengine.StringField()
    avg_score = mongoengine.StringField()
    cmt_latitudes = mongoengine.ListField()
    cmt_picture_list = mongoengine.ListField()
    comment_id = mongoengine.IntField()
    content = mongoengine.StringField()
    user_id = mongoengine.IntField()
    user_img = mongoengine.StringField()
    user_name = mongoengine.StringField()

    vst_name = mongoengine.StringField()
    vst_picture = mongoengine.StringField()
    vst_sp_name = mongoengine.StringField()


class CommentLy(Document):
    dp_content = mongoengine.StringField()
    dp_item_id = mongoengine.StringField()
    dp_id = mongoengine.StringField()
    dp_item_name = mongoengine.StringField()
    # dp_line_access = mongoengine.StringField()
