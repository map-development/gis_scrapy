import mongoengine
import datetime
from mongoengine import Document


class Inventory(Document):
    """
    这里定义的模型， 实际上与使用关系型数据库时定义的模型，其定义的方式是一样的
    只不过使用不同的模块，并继承自不同的父类， 数据保存在不同类型的数据库里。不需要执行迁移操作
    """
    item = mongoengine.StringField(required=True, max_length=125)
    qty = mongoengine.IntField(required=True)
    status = mongoengine.StringField(required=True, max_length=125)
