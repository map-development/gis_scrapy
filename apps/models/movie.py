import scrapy


class MovieItem(scrapy.Item):
    # {'rate': '7.0', 'cover_x': 7142, 'title': '飞驰人生', 'url': 'https://movie.douban.com/subject/30163509/',
    # 'playable': True, 'cover': 'https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2542973862.jpg',
    # 'id': '30163509', 'cover_y': 10000, 'is_new': False}
    rate = scrapy.Field()
    cover_x = scrapy.Field()
    title = scrapy.Field()
    url = scrapy.Field()
    playable = scrapy.Field()
    cover = scrapy.Field()
    id = scrapy.Field()
    cover_y = scrapy.Field()
    is_new = scrapy.Field()
