# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  train.py
@Description    :  制作训练数据集
@CreateTime     :  2021/2/23 2:35 下午
------------------------------------
@ModifyTime     :  
"""

import jieba
import numpy as np
import pandas as pd

# 加载语料库文件，并导入数据
neg = pd.read_excel('data/data/neg.xls', header=None, index_col=None)
pos = pd.read_excel('data/data/pos.xls', header=None, index_col=None)


# print(neg)


# jieba 分词


def word_cut(x): return jieba.lcut(x)


pos['words'] = pos[0].apply(word_cut)
# print(pos['words'])
neg['words'] = neg[0].apply(word_cut)

# 使用 1 表示积极情绪，0 表示消极情绪，并完成数组拼接
x = np.concatenate((pos['words'], neg['words']))
# print(x)
y = np.concatenate((np.ones(len(pos)), np.zeros(len(neg))))
# print(y)
# 将 Ndarray 保存为二进制文件备用
np.save('X_train.npy', x)
np.save('y_train.npy', y)

print('done.')
