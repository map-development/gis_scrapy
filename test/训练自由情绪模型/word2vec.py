# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  word2vec.py
@Description    :  
@CreateTime     :  2021/2/23 2:58 下午
------------------------------------
@ModifyTime     :  
"""
from gensim.models.word2vec import Word2Vec
import warnings
import numpy as np

warnings.filterwarnings('ignore')  # 忽略警告

# 导入上面保存的分词数组
X_train = np.load('X_train.npy', allow_pickle=True)

# 训练 Word2Vec 浅层神经网络模型
w2v = Word2Vec(size=300, min_count=10)
w2v.build_vocab(X_train)
w2v.train(X_train, total_examples=w2v.corpus_count, epochs=w2v.epochs)


def sum_vec(text):
    # 对每个句子的词向量进行求和计算
    vec = np.zeros(300).reshape((1, 300))
    for word in text:
        try:
            vec += w2v[word].reshape((1, 300))
        except KeyError:
            continue
    return vec


# 将词向量保存为 Ndarray
train_vec = np.concatenate([sum_vec(z) for z in X_train])
# 保存 Word2Vec 模型及词向量
w2v.save('w2v_model.pkl')
np.save('X_train_vec.npy', train_vec)
print('done.')
