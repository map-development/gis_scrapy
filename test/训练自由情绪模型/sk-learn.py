# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  sk-learn.py
@Description    :  
@CreateTime     :  2021/2/23 3:03 下午
------------------------------------
@ModifyTime     :  
"""
from sklearn.externals import joblib
from sklearn.tree import DecisionTreeClassifier
import numpy as np

# 导入词向量为训练特征
X = np.load('X_train_vec.npy')
# 导入情绪分类作为目标特征
y = np.load('y_train.npy')
# 构建支持向量机分类模型
model = DecisionTreeClassifier()
# 训练模型
model.fit(X, y)
# 保存模型为二进制文件
joblib.dump(model, 'dt_model.pkl')
