# !/usr/bin/python3
# -*- coding: utf-8 -*-
'''
正负面情绪解决方案
'''

from senta import Senta

my_senta = Senta()

# 获取目前支持的情感预训练模型, 我们开放了以ERNIE 1.0 large(中文)、ERNIE 2.0 large(英文)和RoBERTa large(英文)作为初始化的SKEP模型
print(my_senta.get_support_model())  # ["ernie_1.0_skep_large_ch", "ernie_2.0_skep_large_en", "roberta_skep_large_en"]
#
# 获取目前支持的预测任务
print(my_senta.get_support_task())  # ["sentiment_classify", "aspect_sentiment_classify", "extraction"]
#
# 选择是否使用gpu
use_cuda = False  # 设置True or False

# 预测中文句子级情感分类任务
my_senta.init_model(model_class="ernie_1.0_skep_large_ch", task="sentiment_classify", use_cuda=use_cuda)
texts = ["进门要刷身份证，玩每一个项目都要刷身份证觉得很麻烦","又不可能一直拿手上怕掉，放包里每次都要拿来拿去","代码输入延迟，码字速度有点慢"]
result = my_senta.predict(texts)
print(result)
print(result[0][1])

# # 预测中文评价对象级的情感分类任务
# my_senta.init_model(model_class="ernie_1.0_skep_large_ch", task="aspect_sentiment_classify", use_cuda=use_cuda)
# texts = ["百度是一家高科技公司"]
# aspects = ["百度"]
# result = my_senta.predict(texts, aspects)
# print(result)
#
# 预测中文观点抽取任务
# my_senta.init_model(model_class="ernie_1.0_skep_large_ch", task="extraction", use_cuda=use_cuda)
# texts = ["进门要刷身份证，玩每一个项目都要刷身份证觉得很麻烦，又不可能一直拿手上怕掉，放包里每次都要拿来拿去"]
# result = my_senta.predict(texts)
# print(result)


#
# l1 = [['b'],['c'],['d'],['b'],['c'],['a'],['a']]
# print(l1)
# l2 = list(set([tuple(t) for t in l1]))
# l3 = list([list(t) for t in l2])
# print(l3)
# # test
# l3.sort(key=l1.index)


'''
docker run -it -p 80:80 --name mysite3-nginx \
 -v /home/gis_scrapy/static:/home/gis_scrapy/static \
 -v /home/gis_scrapy/media:/home/gis_scrapy/media \
 -v /home/gis_scrapy/compose/nginx/log:/var/log/nginx \
 -d mynginx:v1
'''