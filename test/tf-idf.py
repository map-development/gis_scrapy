# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  tf-idf.py
@Description    :  
@CreateTime     :  2021/2/20 10:26 上午
------------------------------------
@ModifyTime     :  
"""
'''
云词典解决方案
'''
import jieba.analyse

string = "进门要刷身份证，玩每一个项目都要刷身份证觉得很麻烦，又不可能一直拿手上怕掉，放包里每次都要拿来拿去进门进门进门进门进门进门"
res = jieba.analyse.extract_tags(string, topK=20, withWeight=True)
print(res)
for word, weight in res:  # 得到权重前20的关键词
    print('%s %s' % (word, weight))
