import scrapy
import json

from apps.models import OTA
from apps.models.comment_scrapy import CommentLy
from apps.models.data_scrapy import DataLy


class LySpider(scrapy.Spider):
    name = 'ly'
    allowed_domains = ['www.ly.com']


    start_urls = [
            'http://www.ly.com/scenery/AjaxHelper/DianPingAjax.aspx?action=GetDianPingList&sid=9513&page={1}&pageSize=10&labId=1&sort=0&iid=0.07909997171805272'
        ]
    base_url = r'http://www.ly.com/scenery/AjaxHelper/DianPingAjax.aspx?action=GetDianPingList&sid={ota_spot_id}&page={page_num}&pageSize={page_size}&labId=1&sort=0&iid=0.07909997171805272'
    ota_spot_ids = OTA.OtaSpotIdMap.get_ota_spot_list(OTA.OtaCode.LY)
    page_size = 10

    def parse(self, response):
        # json.loads(response.body)['isSuccess']
        for ota_spot_id in self.ota_spot_ids:
            url = self.base_url.format(ota_spot_id=ota_spot_id, page_num=0, page_size=10)
            print(url)
            page_num = 0

            yield scrapy.Request(url=url
                                 , callback=self.parse_data
                                 , headers={"signal": " ab4494b2-f532-4f99-b57e-7ca121a137ca"}
                                 , dont_filter=True
                                 , meta={'page_num': page_num,
                                         'ota_spot_id': ota_spot_id
                                         })

    def parse_data(self, response):
        page_nums = json.loads(response.body)['pageInfo']['totalPage']
        page_num = 0
        ota_spot_id = response.meta['ota_spot_id']
        while page_num < page_nums:
            page_num += 1
            url = self.base_url.format(ota_spot_id=ota_spot_id, page_num=page_num, page_size=self.page_size)
            yield scrapy.Request(url=url
                                 , callback=self.parse_comment
                                 , headers={"signal": " ab4494b2-f532-4f99-b57e-7ca121a137ca"}
                                 , dont_filter=True
                                 , meta={'page_num': page_num})
            print(url)
            print(page_nums)

    def parse_comment(self, response):
        result = json.loads(response.body)
        print(result['isSuccess'])
        i = 1
        while i < 10:

            dp_item_id = result['dpList'][i]['DPItemId']
            dp_content = result['dpList'][i]['dpContent']
            dp_id = result['dpList'][i]['dpId']
            dp_item_name = result['dpList'][i]['DPItemName']


            # dp_line_access = result['dpList'][0]['lineAccess']

            CommentLy.objects(dp_id=dp_id).update_one(
                set__dp_item_id=dp_item_id,
                set__dp_item_name=dp_item_name,
                set__dp_content=dp_content,
                # set__line_access=dp_line_access,
                upsert=True
            )
            i += 1
        yield None
