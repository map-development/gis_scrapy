# -*- coding: utf-8 -*-
import threading

import scrapy
import re
import json

from apps.models.movie import MovieItem
from apps.service.inventory_service import InventoryService


class MovieSpider(scrapy.Spider):
    name = 'movie'
    allowed_domains = ['movie.douban.com']
    start_urls = [
        'https://movie.douban.com/j/search_subjects?type=movie&tag=热门&sort=recommend&page_limit=20&page_start=0']

    def parse(self, response):
        print(response.url)
        result = json.loads(response.body)
        subjects = result.get('subjects')
        InventoryService.create()
        if len(subjects) > 0:
            for subject in subjects:
                # print(subject)
                yield MovieItem(subject)

