# mysql数据库配置-----------------------------
import mongoengine
import pika

DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'gis_scrapy',
        'USER': 'root',
        'HOST': '11.75.1.54',
        'PASSWORD': '123456',
        'PORT': 3306,
        # 'OPTIONS': {'charset': 'utf8mb4'},
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
        }
    }

}
# mysql数据库配置-----------------------------

# -----------------创建与mongodb数据库的连接
MONGO_CONN = mongoengine.connect(
    db="gis_scrapy",  # 需要进行操作的数据库名称
    alias='default',  # 必须定义一个default数据库
    host="11.75.1.54",
    port=27017,
    username="root",
    password="123456",
    authentication_source="admin",  # 进行身份认证的数据库，通常这个数据库为admin“
)
# -----------------创建与mongodb数据库的连接

# -----------------创建与redis数据库的连接
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://11.75.1.54',
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": "123456",
        },
    },
}
# -----------------创建与redis数据库的连接


# -----------------rabbitmq链接
rabbitmq_connection_parameters = pika.ConnectionParameters(host='11.75.1.54',
                                                           credentials=pika.credentials.PlainCredentials(
                                                               'root', '123456'),
                                                           heartbeat=0  # never exit after start
                                                           )
# -----------------rabbitmq链接

SCRAPY_URL = 'http://11.75.1.54:6800/'
