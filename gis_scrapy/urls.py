"""gis_scrapy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from apps.views import inventory_view
from apps.views import comment_view
# 执行task异步任务--------------
from apps import task

# 执行task异步任务--------------
urlpatterns = [
    path('admin', admin.site.urls),
    path('inventory', inventory_view.index, name='index'),
    # path('test', inventoryView.test, name='test'),
    url('test/$', inventory_view.test),
    path('apis', inventory_view.apis, name='apis_post'),

    path('get/count',comment_view.get_count,name='get_count')
]
