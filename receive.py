# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
@Author         :  cwy
@Version        :  
------------------------------------
@File           :  receive.py
@Description    :  
@CreateTime     :  2021/1/19 3:31 下午
------------------------------------
@ModifyTime     :  
"""
# !/usr/bin/env python
import sys, os
import pika
from gis_scrapy import settings


def main():
    connection = pika.BlockingConnection(settings.rabbitmq_connection_parameters)
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
